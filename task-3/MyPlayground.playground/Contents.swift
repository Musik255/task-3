import UIKit

// Task-3.1

func funcClouser (funtion : () -> ()){

    for i in 0..<10{
        print(i)
    }
    funtion()
}

funcClouser(){
    print("finish")
}






// Task-3.2

arrayNotSorted.sorted()

arrayNotSorted.sorted(by: >)







//Task-3.3


let arrayOfInt : [Int] = [1, 2, 4, 10, 3, 5, 7, 6, 9, 8]

func search(array: [Int], funtion: (Int, Int?)->Bool) -> Int{
    var optionalVariable : Int? = nil
    for i in array {
        if funtion(i, optionalVariable){
            optionalVariable = i
        }
    }
    return optionalVariable!
}

search(array: arrayOfInt , funtion: {
    $1 == nil || $0 < $1!
})

search(array: arrayOfInt , funtion: {
    $1 == nil || $0 > $1!
})
//Делал по ТЗ, можно сделать более оптимизировано, без использования optional переменной, т.к. в любом случае можно сделать присваивание optionalVariable первого элемента массива, с проверкой массива на наличие переменных, а так мы каждый раз проверяем nil там или нет. Согласно BIG O, в сложности алгоритма ничего не поменяется, но все же мы экономим процессорное время.

//Так же, алгоритм выше не учитывает что в массиве может быть 0 элементов, нашел благодаря тестам.

//Исправленный алгоритм

func searchOptimised(array: [Int], funtion: (Int, Int)->Bool) -> String{
    if array.count != 0 {
        var variable : Int = array[0]
        for i in array {
            if funtion(i, variable){
                variable = i
            }
        }
        return String(variable)
    }
    else{
        return String("Используйте другой массив")
    }
}

searchOptimised(array: arrayOfInt, funtion: {
        return $0 > $1
})

searchOptimised(array: arrayOfInt, funtion: {
        return $0 < $1
})

//Очень старнная задача, optional переменная "притянута за уши"









//Task-3.4

func priority(str : String) -> Int{
    switch(str){
    case "a", "e", "i", "o", "u", "y": return 0
    case "A", "E", "I", "O", "U", "Y": return 1
    case "a"..."z": return 2
    case "A"..."Z": return 3
    case "0"..."9": return 4
    default: return 5
    }
}

let a = "1"
let b = "."

switch (priority(str: a), priority(str: b)){
case let(x,y) where x < y: print(a)
case let(x,y) where x > y: print(b)
default: print(a <= b ? a : b)
}


let str = "dafdsbkjatiarbkpu32g87tbFBBHLSEuq34th89qg3ub4o"

var strCharacter = [String]()

for i in str{
    strCharacter.append(String(i))
}



let sorted = strCharacter.sorted{
    switch (priority(str: $0), priority(str: $1)){
    case let(x,y) where x < y: return true
    case let(x,y) where x > y: return false
    default: return $0 <= $1
    }
}

print(sorted)










//Task-3.5

var randomStr = "abdufhbawj;rkag7r92[8hroibaejrkvtpqi"
var randomStrCharacter = [String]()

for i in randomStr{
    randomStrCharacter.append(String(i))
}

func searchOptimisedCharacter(array: [String], funtion: (String, String)->Bool) -> String{
    if array.count != 0 {
        var variable = array[0]
        for i in array {
            if funtion(i, variable){
                variable = i
            }
        }
        return String(variable)
    }
    else{
        return String("Используйте другой массив")
    }
}

print(
    searchOptimisedCharacter(array: randomStrCharacter, funtion: {
        return $0 > $1
    })
)

print(
    searchOptimisedCharacter(array: randomStrCharacter, funtion: {
        return $0 < $1
    })
)








